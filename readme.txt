Projet : Donjon
Auteurs : Nicolas, Marine

Descripition : jeu du donjon

Technologies et outils utilis�es : Java / Maven/ Lombok / Gitkraken / Bitbucket

Environement n�cessaire : Java Runtime Environment 8

D�marche � suivre :
1)Cr�er une variable d'environnement DONJON avec comme valeur le chemin d'acc�s du fichier de sauvegarde des scores (par exemple C:\ENV\donjon\sauvegarde.txt)
1) Sur le r�pertoire C cr�er l�arborescence suivante
(nom du jar).jar se trouve dans le dossier ...

	ENV
   	|------(nom du jar).jar
		


2) Ouvrir l�invite de commande (cmd dans la barre de recherche).
3) D�marrage de l�application, taper les commandes suivantes : 
cd C:\ENV\
java -jar Donjon.jar


Fonctionnement de l'application : 


Un menu s'affiche le joueur a le choix entre :
	- Afficher les 10 meilleurs scores
	- D�marrer une nouvelle partie
	- Quitter

Lorsque le joueur d�cide de d�marrer une nouvelle partie, il doit entrer son nom ainsi que les param�tres de son donjon :
	- La hauteur du labyrinthe (un nombre entre 2 et 30) 
	- La largeur du labyrinthe (un nombre entre 2 et 40) 
	- Le nombre maximum de monstres par salle lors de la cr�ation du donjon ( un nombre entre 0 et 10)
	- Le nombre maximum d'objets par salle lors de la cr�ation du donjon ( un nombre entre 0 et 10)

Une fois les param�tres entr�s, un donjon est g�n�r� al�atoirement, la salle de sortie est repr�sent�e par : ><

Le joueur a le choix entre les actions suivantes : 
	- Afficher la carte d�taill�e (le nombre de monstres et d'objets par salle est affich� sur la carte)
	- Regarder une salle (la salle regard�e doit �tre accessible depuis la salle o� le joueur se trouve), un affichage d�taill� des objets et monstres de la salle est affich�e
	- Attaquer
	- Se deplacer (uniquement si aucun monstre est pr�sent dans la salle)
	- Ramasser un objet (uniquement si aucun monstre est pr�sent dans la salle)


Le joueur gagne la partie s'il a atteint la sortie du donjon
Le joueur perd la partie s'il meurt avant d'atteindre la sortie

Il y a 2 types de monstres dans le layrinthe : 
- 1er type : il se d�place al�atoirement � chaque tour de jeu (un tour correspond aux actions (Regarder, attaquer, deplacer, ramasser).
- 2eme type : il se d�place al�atoirement un tour sur deux, l'autre tour il se d�place vers le joueur (un tour correspond aux actions (Regarder, attaquer, deplacer, ramasser).



Horaires de fonctionnement du programme : 24h/24, 7j/7



