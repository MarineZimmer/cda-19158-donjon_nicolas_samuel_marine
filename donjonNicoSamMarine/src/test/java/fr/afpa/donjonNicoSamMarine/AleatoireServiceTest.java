package fr.afpa.donjonNicoSamMarine;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonNicoSamMarine.services.AleatoireService;
public class AleatoireServiceTest {
	

	@Test
	public void testAleatoireInt() {
	AleatoireService aleatoireService = new AleatoireService();
	int nb = aleatoireService.aleatoireInt(2, 4);
	assertTrue(nb>=2 && nb<=4, "aléatoire int incorrect");
	
	}
}
