package fr.afpa.donjonNicoSamMarine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonNicoSamMarine.entites.Joueur;
import fr.afpa.donjonNicoSamMarine.entites.Monstre;
import fr.afpa.donjonNicoSamMarine.entites.Personnage;
import fr.afpa.donjonNicoSamMarine.services.PersonnageService;

class PersonnageServiceTest {


	@Test
	void testAttaquer() {
		Personnage personnage = new Joueur("billy",12,12,12);
		Personnage personnage2 = new Monstre();
		PersonnageService personnageService = new PersonnageService();
		assertTrue(personnageService.attaquer(personnage, personnage2));
		assertNotNull(personnageService.attaquer(null, personnage2));
		assertNotNull(personnageService.attaquer(personnage, null));
		
	
		
	}

}
