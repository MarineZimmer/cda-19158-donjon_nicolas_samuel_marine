package fr.afpa.donjonNicoSamMarine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonNicoSamMarine.services.FichierService;

public class FichierServiceTest {
	private FichierService fichierService;
	private String cheminFichier;

	@Test
	public void testEcritureFichier() {
		fichierService = new FichierService();
		cheminFichier = "src/test/ressources/sauvegarde.txt";
		assertFalse(fichierService.ecritureFichier(null, "test;tst", true));
		assertFalse(fichierService.ecritureFichier("", "test;tst", true));
		assertTrue(fichierService.ecritureFichier(cheminFichier, "test;tst", true));
	}

	@Test
	public void testLectureFichier() {
		fichierService = new FichierService();
		cheminFichier = "src/test/ressources/sauvegarde.txt";
		fichierService.ecritureFichier(cheminFichier, "test;tst", false);
		fichierService.ecritureFichier(cheminFichier, "test2;tst2", true);
		List<String> liste = new ArrayList<String>();
		liste.add("test;tst");
		liste.add("test2;tst2");
		assertEquals(liste, fichierService.lectureFichier(cheminFichier));
		liste.clear();
		assertEquals(liste, fichierService.lectureFichier("erer"));
	}

}
