package fr.afpa.donjonNicoSamMarine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonNicoSamMarine.entites.BanditManchot;
import fr.afpa.donjonNicoSamMarine.entites.BourseOr;
import fr.afpa.donjonNicoSamMarine.entites.Joueur;
import fr.afpa.donjonNicoSamMarine.entites.Objet;
import fr.afpa.donjonNicoSamMarine.entites.PotionForce;
import fr.afpa.donjonNicoSamMarine.entites.PotionSoin;
import fr.afpa.donjonNicoSamMarine.services.ObjetsService;

public class TestObjetsService {

	private ObjetsService objetsService;
	

	@Test
	public void testCreationObjet() {
		objetsService = new ObjetsService();
		assertNotNull(objetsService.creationObjet(), "Création objet incorrecte");
		assertNotNull(objetsService.creationObjet(), "Création objet incorrecte");
		assertNotNull(objetsService.creationObjet(), "Création objet incorrecte");
		assertNotNull(objetsService.creationObjet(), "Création objet incorrecte");
	}
	
	@Test
	public void testUtilisationObjet() {
		objetsService = new ObjetsService();
		Joueur joueur = new Joueur("test", 10, 20, 30);
		List<Objet> listeObjets = new ArrayList<Objet>();
		listeObjets.add(new PotionForce(5));
		listeObjets.add(new BourseOr(15));
		listeObjets.add(new PotionSoin(10));
		listeObjets.add(new BanditManchot(30));
		assertFalse(objetsService.utilisationObjet(listeObjets, 0, null), "utilisation potion force incorrecte");
		assertFalse(objetsService.utilisationObjet(listeObjets, 1, null), "utilisation potion soin incorrecte");
		assertFalse(objetsService.utilisationObjet(listeObjets, 2, null), "utilisation bourse or incorrecte");
		assertFalse(objetsService.utilisationObjet(listeObjets, 3, null), "utilisation bandit manchot incorrecte");
		assertFalse(objetsService.utilisationObjet(null, 0, joueur), "utilisation bandit manchot incorrecte");
		
		assertTrue(objetsService.utilisationObjet(listeObjets, 0, joueur), "utilisation potion force incorrecte");
		assertEquals(25, joueur.getAttaque());
		assertTrue(objetsService.utilisationObjet(listeObjets, 1, joueur), "utilisation potion soin incorrecte");
		assertEquals(20, joueur.getPointDeVie());
		assertTrue(objetsService.utilisationObjet(listeObjets, 0, joueur), "utilisation bourse or incorrecte");
		assertEquals(45, joueur.getNombrePieceDOr());
		assertTrue(objetsService.utilisationObjet(listeObjets, 0, joueur), "utilisation bandit manchot incorrecte");
		
		
		
		listeObjets.add(new PotionForce(5));
		assertFalse(objetsService.utilisationObjet(listeObjets, 1, joueur), "utilisation bandit manchot incorrecte");
	}
}
