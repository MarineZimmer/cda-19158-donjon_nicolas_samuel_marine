package fr.afpa.donjonNicoSamMarine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonNicoSamMarine.entites.Donjon;
import fr.afpa.donjonNicoSamMarine.entites.Piece;
import fr.afpa.donjonNicoSamMarine.services.DonjonService;
import fr.afpa.donjonNicoSamMarine.services.SalleService;

public class SalleServiceTest {
	@Test
	public void testRegarderSalle() {
		DonjonService donjonService = new DonjonService();
		Donjon donjon = donjonService.creationDonjon();
		donjon.affichage(true);
		SalleService salleService = new SalleService();
		Piece pieceCourante = (Piece) donjon.getLabyrinthe()[2][2];

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Salle Courante"),
				((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX()][pieceCourante.getCoordonneeY()])
						.regarder());

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Haut"),
				((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX() - 1][pieceCourante.getCoordonneeY()])
						.regarder());

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Bas"), "");

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Gauche"), "");

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Droite"),
				((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX()][pieceCourante.getCoordonneeY() + 1])
						.regarder());

		pieceCourante = (Piece) donjon.getLabyrinthe()[1][2];
		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Bas"),
				((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX() + 1][pieceCourante.getCoordonneeY()])
						.regarder());

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Gauche"),
				((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX()][pieceCourante.getCoordonneeY() - 1])
						.regarder());

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, null), "");

		pieceCourante = (Piece) donjon.getLabyrinthe()[1][5];

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Haut"), "");

		assertEquals(salleService.regarderSalle(donjon, pieceCourante, "Droite"), "");
		assertEquals(salleService.regarderSalle(null, pieceCourante, "Droite"), "");
		assertEquals(salleService.regarderSalle(donjon, null, "Droite"), "");

	}

}
