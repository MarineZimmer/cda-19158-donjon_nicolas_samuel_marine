package fr.afpa.donjonNicoSamMarine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonNicoSamMarine.entites.Donjon;
import fr.afpa.donjonNicoSamMarine.entites.Monstre;
import fr.afpa.donjonNicoSamMarine.entites.Piece;
import fr.afpa.donjonNicoSamMarine.entites.Salle;
import fr.afpa.donjonNicoSamMarine.services.DonjonService;
import fr.afpa.donjonNicoSamMarine.services.MonstreService;

public class TestMonstreService {
	private MonstreService monstreService;

	@Test
	public void testCreationMonstre() {
		monstreService = new MonstreService();
		assertNotNull(monstreService.creationMonstre("monstre"), "test creation monstre incorrect");
		assertNotNull(monstreService.creationMonstre("monstre"), "test creation monstre incorrect");
		assertNotNull(monstreService.creationMonstre("monstre"), "test creation monstre incorrect");
	}

	@Test
	public void testMonstreMort() {
		Monstre monstre = new Monstre("Bobby", 2, 5, 10);
		Salle salle = new Piece(0, 0, true, true, true, true, true);
		MonstreService monstreService = new MonstreService();
		((Piece) salle).getListeMonstres().add(monstre);
		assertFalse(monstreService.monstreMort(monstre, salle));
		monstre.setPointDeVie(0);
		assertTrue(monstreService.monstreMort(monstre, salle));
		assertFalse(monstreService.monstreMort(monstre, salle));
		assertFalse(monstreService.monstreMort(null, salle));
		assertFalse(monstreService.monstreMort(monstre, null));

	}

	@Test
	public void testReinitialisationBoolean() {
		monstreService = new MonstreService();
		Donjon donjon = new Donjon(2, 2, 5, 5);
		Monstre monstre = new Monstre("ete", 2, 2, 2);
		Monstre monstre2 = new Monstre("ete", 2, 2, 2);
		if (donjon.getLabyrinthe()[0][1] instanceof Piece) {
			monstre.setDeplacer(true);
			monstre2.setDeplacer(true);
			((Piece) donjon.getLabyrinthe()[0][1]).getListeMonstres().add(monstre);
			((Piece) donjon.getLabyrinthe()[1][1]).getListeMonstres().add(monstre2);
		}
		monstreService.reinitialisationBoolean(donjon);
		assertFalse(monstre.isDeplacer(), "test reinitialisation boolean monstre incorrect");
		assertFalse(monstre2.isDeplacer(), "test reinitialisation boolean monstre incorrect");
	}

	@Test
	public void testDeplacementAleatoire() {
		monstreService = new MonstreService();
		Piece piece = new Piece(0, 0, false, false, false, false, true);
		assertEquals("", monstreService.deplacementMonstreAleatoire(piece));
		piece.setDepB(true);
		assertEquals("Bas", monstreService.deplacementMonstreAleatoire(piece));
		piece.setDepB(false);
		piece.setDepD(true);
		assertEquals("Droite", monstreService.deplacementMonstreAleatoire(piece));
		piece.setDepD(false);
		piece.setDepH(true);
		assertEquals("Haut", monstreService.deplacementMonstreAleatoire(piece));
		piece.setDepH(false);
		piece.setDepG(true);
		assertEquals("Gauche", monstreService.deplacementMonstreAleatoire(piece));

	}

	@Test
	public void testDeplacementIntelligent() {
		monstreService = new MonstreService();
		Donjon donjon = new DonjonService().creationDonjon();
		assertEquals("Gauche", monstreService.deplacementIntelligent(donjon, donjon.getLabyrinthe()[0][0],
				donjon.getLabyrinthe()[2][4]), "deplacement intelligent gauche incorrect");
		assertEquals("Droite", monstreService.deplacementIntelligent(donjon, donjon.getLabyrinthe()[0][2],
				donjon.getLabyrinthe()[1][1]), "deplacement intelligent gauche incorrect");
		assertEquals("Haut", monstreService.deplacementIntelligent(donjon, donjon.getLabyrinthe()[2][4],
				donjon.getLabyrinthe()[1][4]), "deplacement intelligent gauche incorrect");
		assertEquals("Bas", monstreService.deplacementIntelligent(donjon, donjon.getLabyrinthe()[2][3],
				donjon.getLabyrinthe()[1][2]), "deplacement intelligent gauche incorrect");
		assertEquals("", monstreService.deplacementIntelligent(donjon, donjon.getLabyrinthe()[2][4],
				donjon.getLabyrinthe()[2][4]), "deplacement intelligent gauche incorrect");

	}
	@Test
	public void testDeplacerMonstreSalle() {
		DonjonService donjonService = new DonjonService();
		Donjon donjon = donjonService.creationDonjon();
		donjon.affichage(true);
		Monstre monstre = ((Piece)donjon.getLabyrinthe()[1][2]).getListeMonstres().get(0);
		MonstreService monstreService = new MonstreService();
		Piece salleMonstre =((Piece) donjon.getLabyrinthe()[1][2]);
		donjonService.creationDonjon();
		assertTrue(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Haut"),
				"deplacement de monstre incorrect");
		assertTrue(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Bas"),
				"deplacement de monstre incorrect");
		assertTrue(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Gauche"),
				"deplacement de monstre incorrect");
		assertFalse(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Droite"),
				"deplacement de monstre incorrect");
		
		
		monstre = ((Piece)donjon.getLabyrinthe()[2][3]).getListeMonstres().get(0);
		salleMonstre =((Piece) donjon.getLabyrinthe()[2][3]);
		assertFalse(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Haut"),
				"deplacement de monstre incorrect");
		assertFalse(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Bas"),
				"deplacement de monstre incorrect");
		assertTrue(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Gauche"),
				"deplacement de monstre incorrect");
		assertTrue(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Droite"),
				"deplacement de monstre incorrect");
		
		
		monstre = ((Piece)donjon.getLabyrinthe()[2][4]).getListeMonstres().get(0);
		salleMonstre = ((Piece)donjon.getLabyrinthe()[2][4]);
		assertTrue(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Gauche"),
				"deplacement monstre incorrect");
		assertFalse(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Droite"),
				"deplacement monstre incorrect");
		assertFalse(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Haut"),
				"deplacement monstre incorrect");
		assertFalse(monstreService.deplacerMonstreSalle(donjon, monstre, salleMonstre, "Bas"),
				"deplacement monstre incorrect");
		
		
	}

@Test
public void testDeplacementEnsembleMonstre() {
	MonstreService monstreService = new MonstreService();
	DonjonService donjonService = new DonjonService();
	Donjon donjon = donjonService.creationDonjon();
	donjon.affichage(true);
	donjon.setDeplacementMonstre(true);
	Piece salleJoueur = ((Piece) donjon.getLabyrinthe()[0][0]);
	assertTrue(monstreService.deplacementEnsembleMonstre(donjon, salleJoueur),"Deplacement incorrect");
	System.out.println(donjon.getNbTour());
	assertTrue(monstreService.deplacementEnsembleMonstre(donjon, salleJoueur),"Deplacement incorrect");
	System.out.println(donjon.getNbTour());
	donjon.setDeplacementMonstre(false);
	assertFalse(monstreService.deplacementEnsembleMonstre(donjon, salleJoueur),"Deplacement incorrect");
	assertFalse(monstreService.deplacementEnsembleMonstre(null, salleJoueur));
	assertFalse(monstreService.deplacementEnsembleMonstre(donjon, null));
}

}
