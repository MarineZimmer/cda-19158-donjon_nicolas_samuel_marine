package fr.afpa.donjonNicoSamMarine.iservices;


import fr.afpa.donjonNicoSamMarine.entites.Personnage;

/**
 * Service d'attaque d'un personnage
 * @param personnageAttaque : le personnage qui attaque
 * @param personnageTouche : le personnage qui est attaqué
 * @return true si l'attaque a été réalisée
 */
public interface ICombattre {
	public  boolean attaquer(Personnage personnageAttaque, Personnage personnageTouche);
}
