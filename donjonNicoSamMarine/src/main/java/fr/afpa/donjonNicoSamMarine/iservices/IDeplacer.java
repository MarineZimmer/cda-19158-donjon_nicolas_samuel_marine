package fr.afpa.donjonNicoSamMarine.iservices;

import fr.afpa.donjonNicoSamMarine.entites.Donjon;
import fr.afpa.donjonNicoSamMarine.entites.Salle;

public interface IDeplacer {

	/**
	 * Service de déplacement d'un joueur, déplace le joueur d'une salle à une autre
	 * 
	 * @param donjon        : le donjon
	 * @param salleCourante : la salle où se situe le joueur
	 * @param deplacement   : le deplacement effectué
	 * @return la nouvelle salle où se situe le joueur
	 */
	Salle deplacer(Donjon donjon, Salle salleCourante, String deplacement);
}
