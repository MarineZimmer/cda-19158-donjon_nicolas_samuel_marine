package fr.afpa.donjonNicoSamMarine.entites;

public class SuperMonstre extends Monstre{

	
	public SuperMonstre() {
		super();
	}

	public SuperMonstre(String nom, int pointDeVie, int attaque, int nombrePieceDOr) {
		super(nom, pointDeVie, attaque, nombrePieceDOr);
	}

	@Override
	public String toString() {
		return "SuperMonstre [Nom =" + getNom() + ", Point de vie=" + getPointDeVie() + ", attaque="
				+ getAttaque() + ", piece d'or=" + getNombrePieceDOr() + "]";
	}

}
