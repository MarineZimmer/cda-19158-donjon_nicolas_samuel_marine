package fr.afpa.donjonNicoSamMarine.entites;

import fr.afpa.donjonNicoSamMarine.ientites.IUtiliser;

public class PotionForce extends Objet implements IUtiliser{

	/**
	 * @param valeur
	 */
	public PotionForce(int valeur) {
		super(valeur);
	
	}

	public PotionForce() {
		super();
	}

	@Override
	public String toString() {
		return "PotionForce [valeur =" + super.getValeur() + "]";
	}

	@Override
	public boolean utiliserObjet(Personnage personnage) {
		if(personnage!=null) {
			System.out.println("Utilisation de l'objet potion force d'une valeur de " + this.getValeur());
			personnage.setAttaque(personnage.getAttaque()+this.getValeur());
			return true;
		}
		return false;
	}

}
