package fr.afpa.donjonNicoSamMarine.entites;

public class PieceDepart extends Piece {

	public PieceDepart() {
		super();
	}

	public PieceDepart(int coordonneeX, int coordonneeY, boolean depB, boolean depH, boolean depG, boolean depD,
			boolean decouverte) {
		super(coordonneeX, coordonneeY, depB, depH, depG, depD, decouverte);
	}
	
	

}
