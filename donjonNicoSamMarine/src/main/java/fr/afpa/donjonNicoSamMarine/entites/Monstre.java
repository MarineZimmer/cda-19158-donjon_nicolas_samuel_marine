package fr.afpa.donjonNicoSamMarine.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Monstre extends Personnage {
	
	private boolean deplacer;
	@Getter
	private static int forceMax=20;
	@Getter
	private static int pointVieMax=50;
	@Getter
	private static int nombrePieceOrMax=20;
	@Getter
	private static int forceMin = 5;
	@Getter
	private static int pointVieMin = 20;
	@Getter
	private static int nombrePieceOrMinDepart = 5;
	
	
	public Monstre() {
		super();
	}

	/**
	 * @param nom
	 * @param pointDeVie
	 * @param attaque
	 * @param nombrePieceDOr
	 */
	public Monstre(String nom, int pointDeVie, int attaque, int nombrePieceDOr) {
		super(nom, pointDeVie, attaque, nombrePieceDOr);
	}


	@Override
	public String toString() {
		return "Monstre [Nom =" + getNom() + ", Point de vie=" + getPointDeVie() + ", attaque="
				+ getAttaque() + ", piece d'or=" + getNombrePieceDOr() + "]";
	}

}
