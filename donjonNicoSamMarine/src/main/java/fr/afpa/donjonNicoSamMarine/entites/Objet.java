package fr.afpa.donjonNicoSamMarine.entites;

import fr.afpa.donjonNicoSamMarine.ientites.IUtiliser;
import lombok.Getter;

@Getter
public abstract class Objet implements IUtiliser {
	private int valeur;
	/**
	 * 
	 */
	public Objet() {
		super();
	
	}
	

	/**
	 * @param valeur
	 */
	public Objet(int valeur) {
		super();
		this.valeur = valeur;
	}

}
