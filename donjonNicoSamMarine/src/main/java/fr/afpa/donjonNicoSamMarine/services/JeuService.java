package fr.afpa.donjonNicoSamMarine.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.afpa.donjonNicoSamMarine.entites.Piece;
import fr.afpa.donjonNicoSamMarine.entites.Score;

/**
 * 
 * 
 * @author 59013-70-11
 *
 */
public class JeuService {

	/**
	 * Fonction permettant d'afficher une liste des actions possibles pour le
	 * joueur, selon les caracteristiques d'une salle
	 * 
	 * @param piece : la piece où se situe le joueur
	 * @return : une liste des actions possibles du joueur
	 */
	public List<String> actionJoueur(Piece piece) {

		List<String> action = new ArrayList<String>();
		action.add("Affichage de la carte en detail");
		action.add("Regarder");
		if (!piece.getListeMonstres().isEmpty()) {
			action.add("Attaquer");
		} else {
			action.add("Deplacer");
			if (!piece.getListeObjets().isEmpty()) {
				action.add("Objets");
			}
		}
		return action;
	}

	/**
	 * Fonction permettant d'afficher les possibilités de déplacement du joueur
	 * lorqu'il se situe dans une salle du donjon
	 * 
	 * @param piece : La piece où se situe le joueur
	 * @return une liste contenant les déplacements possible du joueur
	 */
	public List<String> deplacementPossibleJoueur(Piece piece) {

		List<String> deplacement = new ArrayList<String>();

		if (piece.isDepH() == true) {
			deplacement.add("Haut");
		}
		if (piece.isDepB() == true) {
			deplacement.add("Bas");
		}
		if (piece.isDepG() == true) {
			deplacement.add("Gauche");
		}
		if (piece.isDepD() == true) {
			deplacement.add("Droite");
		}

		return deplacement;

	}

	/**
	 * retourne la liste des meilleurs scores de tous les joueurs
	 * 
	 * @param nbScore : le nombre de scores que la liste doit contenir
	 * @return : la liste des meilleurs scores
	 */
	public List<Score> topScores(int nbScore, String cheminFichierScores) {
		FichierService fichierService = new FichierService();
		List<Score> listeScore = new ArrayList<>();
		List<String> listeScoreFichier = fichierService.lectureFichier(cheminFichierScores);
		String[] ligne;
		for (String ligneFichier : listeScoreFichier) {
			ligne = ligneFichier.split(";");
			if (ligne.length == 3 && ligne[2].matches("-?[0-9]+")) {
				listeScore.add(new Score(Integer.parseInt(ligne[2]), ligne[0], ligne[1]));
			}
		}
		Collections.sort(listeScore);
		if (listeScore.size() > nbScore) {
			return listeScore.subList(0, nbScore);
		} else {
			return listeScore;
		}
	}
}
