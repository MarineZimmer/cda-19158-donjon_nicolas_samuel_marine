package fr.afpa.donjonNicoSamMarine.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import fr.afpa.donjonNicoSamMarine.controles.ControleSaisie;
import fr.afpa.donjonNicoSamMarine.entites.Donjon;
import fr.afpa.donjonNicoSamMarine.entites.Joueur;
import fr.afpa.donjonNicoSamMarine.entites.Piece;
import fr.afpa.donjonNicoSamMarine.entites.PieceArriver;
import fr.afpa.donjonNicoSamMarine.entites.Salle;
import fr.afpa.donjonNicoSamMarine.iservices.IDeplacer;

public class JoueurService implements IDeplacer {
	/**
	 * Fonction permettant la creation aléatoire des paramètres du joueur
	 * 
	 * @param nom : le nom du joueur
	 * @return : un joueur créé aléatoirement
	 */
	public Joueur creationJoueur(String nom) {
		AleatoireService a = new AleatoireService();

		return new Joueur(nom, a.aleatoireInt(Joueur.getPointVieMin(), Joueur.getPointVieMax()),
				a.aleatoireInt(Joueur.getForceMin(), Joueur.getForceMax()),
				a.aleatoireInt(Joueur.getNombrePieceOrMinDepart(), Joueur.getNombrePieceOrMaxDepart()));

	}

	/**
	 * Fonction permettant de vérifier si l'utilisateur a perdu ou non à l'aide du
	 * nombre de points de vie restants
	 * 
	 * @param salle : la salle où se situe le joueur
	 * @return true si le jeu continu, false sinon (victoire ou défaite)
	 */
	public boolean jeuContinu(Salle salle) {
		if (salle instanceof Piece) {
			if (((Piece) salle).getJoueur() != null && ((Piece) salle).getJoueur().getPointDeVie() <= 0) {
				System.out.println("Perdu!!");
				return false;
			}
			if (salle instanceof PieceArriver) {
				if (((Piece) salle).getJoueur() != null) {
					enregistrementScore(((Piece) salle).getJoueur(),System.getenv("DONJON"));
					System.out.println("Gagné!! score : " + ((Piece) salle).getJoueur().getNombrePieceDOr());
				}
				return false;
			}
		}
		return true;

	}

	@Override
	public Salle deplacer(Donjon donjon, Salle salleCourante, String deplacement) {
		Joueur joueur = ((Piece) salleCourante).getJoueur();
		switch (deplacement) {
		case "Haut":
			if (salleCourante.isDepH()) {
				((Piece) donjon.getLabyrinthe()[salleCourante.getCoordonneeX() - 1][salleCourante.getCoordonneeY()])
						.setJoueur(joueur);
				((Piece) salleCourante).setJoueur(null);
				return donjon.getLabyrinthe()[salleCourante.getCoordonneeX() - 1][salleCourante.getCoordonneeY()];
			}
			break;
		case "Bas":
			if (salleCourante.isDepB()) {
				((Piece) donjon.getLabyrinthe()[salleCourante.getCoordonneeX() + 1][salleCourante.getCoordonneeY()])
						.setJoueur(joueur);
				((Piece) salleCourante).setJoueur(null);
				return donjon.getLabyrinthe()[salleCourante.getCoordonneeX() + 1][salleCourante.getCoordonneeY()];
			}
			break;
		case "Gauche":
			if (salleCourante.isDepG()) {
				((Piece) donjon.getLabyrinthe()[salleCourante.getCoordonneeX()][salleCourante.getCoordonneeY() - 1])
						.setJoueur(joueur);
				((Piece) salleCourante).setJoueur(null);
				return donjon.getLabyrinthe()[salleCourante.getCoordonneeX()][salleCourante.getCoordonneeY() - 1];
			}
			break;
		case "Droite":
			if (salleCourante.isDepD()) {
				((Piece) donjon.getLabyrinthe()[salleCourante.getCoordonneeX()][salleCourante.getCoordonneeY() + 1])
						.setJoueur(joueur);
				((Piece) salleCourante).setJoueur(null);
				return donjon.getLabyrinthe()[salleCourante.getCoordonneeX()][salleCourante.getCoordonneeY() + 1];
			}

			break;
		}
		return null;
	}

	/**
	 * Enregistrement du score dans le fichier texte de sauvegarde
	 * 
	 * @param joueur : le Joueur qui a gagné
	 * @param	cheminFichier : le chemin du fichier de sauvegarde
	 * @return true si la sauvegarde est réussite, false sinon 
	 */
	public boolean enregistrementScore(Joueur joueur, String cheminFichier) {
		if (joueur != null && ControleSaisie.saisieNom(joueur.getNom())) {
			FichierService fichierService = new FichierService();
			return fichierService.ecritureFichier(cheminFichier,
					LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + ";" + joueur.getNom() + ";"
							+ joueur.getNombrePieceDOr(),
					true);
	
		}
		return false;
	}

}
