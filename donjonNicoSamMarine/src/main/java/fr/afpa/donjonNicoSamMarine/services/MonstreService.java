package fr.afpa.donjonNicoSamMarine.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import fr.afpa.donjonNicoSamMarine.entites.BourseOr;
import fr.afpa.donjonNicoSamMarine.entites.Donjon;
import fr.afpa.donjonNicoSamMarine.entites.Monstre;
import fr.afpa.donjonNicoSamMarine.entites.Piece;
import fr.afpa.donjonNicoSamMarine.entites.PieceArriver;
import fr.afpa.donjonNicoSamMarine.entites.Salle;
import fr.afpa.donjonNicoSamMarine.entites.SuperMonstre;

public class MonstreService {
	/**
	 * Création aléatoire des paramètres d'un monstre
	 * 
	 * @param nom : le nom du monstre
	 * @return : le Monstre créé aléatoirement
	 */
	public Monstre creationMonstre(String nom) {
		AleatoireService a = new AleatoireService();
		if (new Random().nextBoolean()) {
			return new Monstre(nom, a.aleatoireInt(Monstre.getPointVieMin(), Monstre.getPointVieMax()),
					a.aleatoireInt(Monstre.getForceMin(), Monstre.getForceMax()),
					a.aleatoireInt(Monstre.getNombrePieceOrMinDepart(), Monstre.getNombrePieceOrMax()));
		} else {
			return new SuperMonstre(nom, a.aleatoireInt(Monstre.getPointVieMin(), Monstre.getPointVieMax()),
					a.aleatoireInt(Monstre.getForceMin(), Monstre.getForceMax()),
					a.aleatoireInt(Monstre.getNombrePieceOrMinDepart(), Monstre.getNombrePieceOrMax()));
		}
	}

	/**
	 * verifie si le monstre est mort, si il est mort il jete une bourse d'or
	 * 
	 * @param monstre       : le monstre a controler si il est vivant ou non
	 * @param salleCourante : la salle où se trouve le monstre
	 * @return true si le monstre est mort, false sinon
	 */
	public boolean monstreMort(Monstre monstre, Salle salleCourante) {
		if (monstre != null && salleCourante != null) {
			if (monstre.getPointDeVie() <= 0 && salleCourante instanceof Piece
					&& ((Piece) salleCourante).getListeMonstres().contains(monstre)) {
				((Piece) salleCourante).getListeObjets().add(new BourseOr(monstre.getNombrePieceDOr()));
				((Piece) salleCourante).getListeMonstres().remove(monstre);
				return true;
			}
		}
		return false;
	}

	/**
	 * Methode permettant de donner la capacité à un monstre ou non de se deplacer
	 * 
	 * @param donjon : emplacement ou se trouve le labyrinthe
	 * @return : true si des monstres ont étés trouvés dans une piece du labyrinthe
	 */
	public boolean reinitialisationBoolean(Donjon donjon) {

		if (donjon != null) {
			for (int i = 0; i < donjon.getLabyrinthe().length; i++) {
				for (int j = 0; j < donjon.getLabyrinthe()[0].length; j++) {
					if (donjon.getLabyrinthe()[i][j] instanceof Piece
							&& !((Piece) donjon.getLabyrinthe()[i][j]).getListeMonstres().isEmpty()) {
						Iterator<Monstre> it = ((Piece) donjon.getLabyrinthe()[i][j]).getListeMonstres().iterator();
						while (it.hasNext()) {
							Monstre monstre = it.next();
							monstre.setDeplacer(false);
						}
					}
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Methode generant un deplacement aléatoire des monstres
	 * 
	 * @param piece : l'endroit ou se trouve le monstre
	 * @return : deplacement géneré aléatoirement du monstre
	 */
	public String deplacementMonstreAleatoire(Piece piece) {

		List<String> listDeplacementMonstre = new ArrayList<String>();
		if (piece.isDepH() == true) {
			listDeplacementMonstre.add("Haut");
		}
		if (piece.isDepB() == true) {
			listDeplacementMonstre.add("Bas");
		}
		if (piece.isDepG() == true) {
			listDeplacementMonstre.add("Gauche");
		}
		if (piece.isDepD() == true) {
			listDeplacementMonstre.add("Droite");
		}
		Collections.shuffle(listDeplacementMonstre);
		if (!listDeplacementMonstre.isEmpty()) {
			return listDeplacementMonstre.get(0);
		} else {
			return "";
		}
	}

	/**
	 * Fonction faisant avancer les monstres vers le joueur au cours d'une partie
	 * 
	 * @param donjon       : Le donjon
	 * @param salleJoueur  : la salle ou le joueur se trouve
	 * @param salleMonstre : la salle ou le monstre se trouve
	 * @return : la direction que doit prendre le monstre pour avancer vers le
	 *         joueur
	 */
	public String deplacementIntelligent(Donjon donjon, Salle salleJoueur, Salle salleMonstre) {
		if (donjon != null && salleJoueur != null && salleMonstre != null) {

			Salle[][] lab = donjon.getLabyrinthe();
			int x = salleJoueur.getCoordonneeX();
			int y = salleJoueur.getCoordonneeY();
			char dep = 'H';

			if (salleJoueur.equals(salleMonstre)) {
				return "";
			}
			while (!lab[x][y].equals(salleMonstre)) {
				switch (dep) {
				case 'H':
					if (lab[x][y].isDepG()) {
						dep = 'G';
					} else if (lab[x][y].isDepH()) {
						dep = 'H';
					} else if (lab[x][y].isDepD()) {
						dep = 'D';
					} else if (lab[x][y].isDepB()) {
						dep = 'B';
					}
					break;
				case 'D':
					if (lab[x][y].isDepH()) {
						dep = 'H';
					} else if (lab[x][y].isDepD()) {
						dep = 'D';
					} else if (lab[x][y].isDepB()) {
						dep = 'B';
					} else if (lab[x][y].isDepG()) {
						dep = 'G';
					}
					break;
				case 'B':
					if (lab[x][y].isDepD()) {
						dep = 'D';
					} else if (lab[x][y].isDepB()) {
						dep = 'B';
					} else if (lab[x][y].isDepG()) {
						dep = 'G';
					} else if (lab[x][y].isDepH()) {
						dep = 'H';
					}

					break;
				case 'G':
					if (lab[x][y].isDepB()) {
						dep = 'B';
					} else if (lab[x][y].isDepG()) {
						dep = 'G';
					} else if (lab[x][y].isDepH()) {
						dep = 'H';
					} else if (lab[x][y].isDepD()) {
						dep = 'D';
					}
					break;
				default:
					break;
				}
				if (dep == 'H') {
					x--;
				} else if (dep == 'D') {
					y++;
				} else if (dep == 'B') {
					x++;
				} else if (dep == 'G') {
					y--;
				}
			}
			switch (dep) {
			case 'H':
				return "Bas";
			case 'B':
				return "Haut";
			case 'G':
				return "Droite";
			case 'D':
				return "Gauche";
			}

		}
		return "";
	}

	/**
	 * Service d'ajout d' un monstre dans une salle (la suppression du monstre de
	 * son ancienne salle se fait dans le service de deplacementEnsembleMonstre)
	 * 
	 * @param donjon       : le donjon
	 * @param monstre      : le monstre à deplacer
	 * @param salleMonstre : la salle où le monstre se trouve
	 * @param deplacement  : le sens de deplacement du monstre(Haut, Bas,
	 *                     Gauche,Droite)
	 * @return true si l'ajout a été effectué, false sinon
	 */
	public boolean deplacerMonstreSalle(Donjon donjon, Monstre monstre, Piece salleMonstre, String deplacement) {
		if (salleMonstre != null && donjon != null && deplacement != null) {
			switch (deplacement) {
			case "Haut":
				if (salleMonstre.isDepH()
						&& !((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX() - 1][salleMonstre
								.getCoordonneeY()] instanceof PieceArriver)) {
					((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX() - 1][salleMonstre.getCoordonneeY()])
							.getListeMonstres().add(monstre);
					return true;
				}
				break;
			case "Bas":
				if (salleMonstre.isDepB()
						&& !((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX() + 1][salleMonstre
								.getCoordonneeY()] instanceof PieceArriver)) {
					((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX() + 1][salleMonstre.getCoordonneeY()])
							.getListeMonstres().add(monstre);
					return true;
				}
				break;
			case "Gauche":
				if (salleMonstre.isDepG()
						&& !((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX()][salleMonstre.getCoordonneeY()
								- 1] instanceof PieceArriver)) {
					((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX()][salleMonstre.getCoordonneeY() - 1])
							.getListeMonstres().add(monstre);
					return true;
				}
				break;
			case "Droite":
				if (salleMonstre.isDepD()
						&& !((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX()][salleMonstre.getCoordonneeY()
								+ 1] instanceof PieceArriver)) {
					((Piece) donjon.getLabyrinthe()[salleMonstre.getCoordonneeX()][salleMonstre.getCoordonneeY() + 1])
							.getListeMonstres().add(monstre);
					return true;
				}
			default:
			}
		}
		return false;
	}

	/**
	 * Service de deplacement de l'ensemble des monstres du donjon
	 * 
	 * @param donjon      : le donjon
	 * @param salleJoueur : la salle où le joueur se trouve
	 * @return true si le deplacement des monstres s'est bien passé
	 */
	public boolean deplacementEnsembleMonstre(Donjon donjon, Salle salleJoueur) {
		String deplacementAleatoire = "";
		String deplacementIntelligent = "";
		Monstre monstre;

		if (donjon != null && salleJoueur instanceof Piece && donjon.isDeplacementMonstre()) {

			donjon.setNbTour(donjon.getNbTour() + 1);

			reinitialisationBoolean(donjon);

			Salle[][] lab = donjon.getLabyrinthe();

			for (int i = 0; i < lab.length; i++) {
				for (int j = 0; j < lab[0].length; j++) {

					if (lab[i][j] instanceof Piece && !((Piece) lab[i][j]).getListeMonstres().isEmpty()
							&& (lab[i][j].isDepB() || lab[i][j].isDepD() || lab[i][j].isDepG() || lab[i][j].isDepH())) {

						if (donjon.getNbTour() % 2 == 0) {
							deplacementIntelligent = deplacementIntelligent(donjon, salleJoueur, lab[i][j]);						}

						Iterator<Monstre> it = ((Piece) lab[i][j]).getListeMonstres().iterator();
						
						while (it.hasNext()) {
							monstre = it.next();
							if (!monstre.isDeplacer()) {
								monstre.setDeplacer(true);
								if (monstre instanceof SuperMonstre && donjon.getNbTour() % 2 == 0) {
									if (deplacerMonstreSalle(donjon, monstre, ((Piece) lab[i][j]),
											deplacementIntelligent)) {
										it.remove();
									}

								} else {
									deplacementAleatoire = deplacementMonstreAleatoire((Piece) lab[i][j]);
									if (deplacerMonstreSalle(donjon, monstre, ((Piece) lab[i][j]),
											deplacementAleatoire)) {
										it.remove();
									}
								}
							}
						}

					}
				}

			}
			return true;
		}
		return false;

	}

}
