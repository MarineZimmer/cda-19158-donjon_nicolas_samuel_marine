package fr.afpa.donjonNicoSamMarine.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FichierService {
	/**
	 * Service permettant la sauvegarde des scores du joueur sur un fichier défini
	 * dans une variable d'environnement systeme
	 * 
	 * @param file : le chemin du fichier 
	 * @param score : la ligne à écrire
	 * @param ecriture : true si on rajoute la ligne au fichier, false si on écrase le fichier
	 */
	public boolean ecritureFichier(String file, String score, boolean ecriture) {
		boolean flag = true;
		BufferedWriter bw = null;
		try {
			File fichier = new File(file);

			File dir = fichier.getParentFile();

			dir.mkdirs();
			FileWriter fw = new FileWriter(file, ecriture);
			bw = new BufferedWriter(fw);

			bw.write(score);
			bw.newLine();
			

		} 
		catch (Exception e) {
			flag = false;
			System.out.println("Probleme ecriture fichier");
		}
		finally {
		if(bw != null){
			try {
		
				bw.close();
			} catch (IOException e) {
				flag = false;
				System.out.println("Probleme ecriture fichier");
			}
		}
		}
		return flag ;
		
	}

	/**
	 * Service permettant la lecture du fichier de sauvegarde, permettant d'afficher
	 * les scores et le nom du joueur
	 * 
	 * @param inputFile : le chemin du fichier
	 * @return : une liste contenant les lignes du fichier
	 */
	public List<String> lectureFichier(String inputFile) {

		BufferedReader br = null;
		List<String> listScore = new ArrayList<String>();
		try {
			FileReader fr = new FileReader(inputFile);
			br = new BufferedReader(fr);

			while (br.ready()) {
				listScore.add(br.readLine());

			}
		} catch (Exception e) {
			System.out.println("Erreur lecture");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("Erreur fermeture buffer");
				}
			}
		}
		return listScore;
	}

}
