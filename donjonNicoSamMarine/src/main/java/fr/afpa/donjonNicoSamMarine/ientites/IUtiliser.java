package fr.afpa.donjonNicoSamMarine.ientites;

import fr.afpa.donjonNicoSamMarine.entites.Personnage;

public interface IUtiliser {
	/**
	 * Utilisation d'un objet, augmente les points correspondant à l'objet
	 * @param personnage : le Personnage qui utilse
	 * @return true si l'objet a été utilisé
	 */
boolean utiliserObjet( Personnage personnage);

}
