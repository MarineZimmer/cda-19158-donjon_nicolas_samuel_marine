package fr.afpa.donjonNicoSamMarine.ihms;

import java.util.Scanner;

import fr.afpa.donjonNicoSamMarine.controles.ControleSaisie;

public class IhmSaisie {
	
	/**
	 * retourne la ligne saisie par l'utilisateur
	 * @return : la ligne saisie par l'utilisateur
	 */
	public String saisieUtilisateur() {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}
	
	/**
	 * retourne un entier saisi entre le min et le max, redemande une saisie tant que la réponse est incorrecte
	 * @param min : le minimum
	 * @param max : le maximum
	 * @return : un entier entre le min et le max
	 */
	public int saisieInt(int min, int max) {
		
		String saisieInt;
		
		do {
			saisieInt = saisieUtilisateur();
			if(ControleSaisie.saisieInt(saisieInt, min,max)) {
				break;
			}else {
				System.out.println("Erreur ,saisissez un entier entre " + min + " et " + max);
			}
			
		}while(true);
		
		return Integer.parseInt(saisieInt);
	}

	/**
	 * retourne le nom saisi, redemande une saisie tant que la saisie est vide ou composée uniquement d'espace
	 * @return : une chaine de caractères non vide 
	 */
	public String SaisieNom() {
		String nom;
		do {
			nom=saisieUtilisateur();
			if(ControleSaisie.saisieNom(nom)) {
				break;
			}else {
				System.out.println("Erreur, entrer un nom non vide ");
			}
		}while(true);
		return nom;
	}
}
