package fr.afpa.donjonNicoSamMarine.ihms;

import fr.afpa.donjonNicoSamMarine.entites.Donjon;
import fr.afpa.donjonNicoSamMarine.services.DonjonService;

public class IhmDonjon {
	/**
	 * Fonction permettant l'initialisation et le parametrage du Donjon
	 * 
	 * @return le dojon créé aléatoirement
	 */
	public Donjon creationDonjon() {
		IhmSaisie ihmSaisie = new IhmSaisie();
		DonjonService donjonService = new DonjonService();
		int hauteur;
		int largeur;
		int nbMonstreMaxSalle;
		int nbObjetMaxSalle;
		System.out.println("Entrer la hauteur du labyrinthe : ");
		hauteur = ihmSaisie.saisieInt(2, 30);
		System.out.println("Entrer la largeur du labyrinthe : ");
		largeur = ihmSaisie.saisieInt(2, 40);
		System.out.println("Entrer le nombre max de monstres par salle : ");
		nbMonstreMaxSalle = ihmSaisie.saisieInt(0, 10);
		System.out.println("Entrer le nombre max d'objets par salle : ");
		nbObjetMaxSalle = ihmSaisie.saisieInt(0, 10);
		return donjonService.creationDonjonAleatoire(hauteur, largeur, nbMonstreMaxSalle, nbObjetMaxSalle);
	}

}
